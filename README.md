# Test Angular

Ce projet vise à sélectionner un département via une TextBox et à afficher la liste des départements qui font partie de la même région que le département sélectionnée

## Test unitaire

Le test unitaire est de récupérer tous les départements afin de s'assurer que l'API fonctionne et que l'URL est correcte.
Le résultat attendu est que la liste des départements n'est pas vide.
