import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { GeoApiComponent, Department } from './geo-api.component';
import { GeoApiService } from 'src/app/services/geo-api.service';
import { HttpClientModule } from '@angular/common/http';
import { of } from 'rxjs';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

describe('GeoApiComponent', () => {
  let component: GeoApiComponent;
  let fixture: ComponentFixture<GeoApiComponent>;
  let geoApiService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeoApiComponent ],
      providers : [ GeoApiService ],
      imports: [ HttpClientModule, MatAutocompleteModule ]
    })
    .compileComponents();
  }));

  beforeEach(inject([GeoApiService], s => {
    geoApiService = s;
    fixture = TestBed.createComponent(GeoApiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should receive all departments', async(() => {
    const response: Department[] = [];
    spyOn(geoApiService, 'getAllDepartments').and.returnValue(of(response))
    component.getDepartmentsList();
    fixture.detectChanges();
    expect(response.length > 0);
  }));
});
