import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GeoApiComponent } from './geo-api.component';

const routes: Routes = [
  {
    path: '',
    component: GeoApiComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GeoApiRoutingModule { }
