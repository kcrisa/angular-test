import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeoApiRoutingModule } from './geo-api-routing.module';
import { GeoApiComponent } from './geo-api.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatListModule } from '@angular/material/list';



@NgModule({
  declarations: [
    GeoApiComponent
  ],
  imports: [
    CommonModule,
    GeoApiRoutingModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatListModule
  ]
})
export class GeoApiModule { }
