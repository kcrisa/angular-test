import { Component, OnInit } from '@angular/core';
import { GeoApiService } from 'src/app/services/geo-api.service';
import * as _ from 'lodash';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';

export interface Department {
  name: string;
  code: string;
  regionCode: string;
}

@Component({
  selector: 'app-geo-api',
  templateUrl: './geo-api.component.html',
  styleUrls: ['./geo-api.component.scss']
})
export class GeoApiComponent implements OnInit {
  departmentCtrl = new FormControl();
  filteredDepartment: Observable<any[]>;
  departmentsList: Department[] = [];
  departmentsSameRegionList: Department[] = [];
  departmentSelected: boolean = false;

  constructor(
    private geoApiService: GeoApiService
  ) {
    this.filteredDepartment = this.departmentCtrl.valueChanges
      .pipe(
        startWith(''),
        map(department => department ? this._filterDepartments(department) : this.departmentsList.slice())
      );
  }

  ngOnInit(): void {
    this.getDepartmentsList();
  }

  private _filterDepartments(value: any): Department[] {
    this.departmentSelected = false;
    const filterValue = value.name ? value.name.toLowerCase() : value.toLowerCase();
    return this.departmentsList.filter(department => department.name.toLowerCase().indexOf(filterValue) === 0);
  }

  // Pour l'autocomplétion, on récupère tous les départements en une fois pour limiter les appels
  // https://geo.api.gouv.fr/departements
  getDepartmentsList() {
    this.geoApiService.getAllDepartments().subscribe(
      resultAllDepartments => {
        const departmentsNameAndRegionCodeList: Department[] = [];
        _.forEach(resultAllDepartments, department => {
          departmentsNameAndRegionCodeList.push({
            name: department.nom,
            code: department.code,
            regionCode: department.codeRegion
          })
        })
        this.departmentsList = [...departmentsNameAndRegionCodeList];
      },
      error => {
        console.error("Impossible to have all departments : " + error);
      },
    )
  }

  // Méthode pour récupérer les départements de la même région du département sélectionné
  getDepartmentsForSameRegion(departmentName: any) {
    this.departmentSelected = true;
    const foundDepartment = _.find(this.departmentsList, {
      name: departmentName,
    });
    const regionCodeOfDepartment: string = foundDepartment.regionCode;
    this.geoApiService.getDepartmentsFromRegionCode(regionCodeOfDepartment).subscribe(
      resultDepartmentsFromRegionCode => {
        const departmentsSameRegion: Department[] = [];
        _.forEach(resultDepartmentsFromRegionCode, department => {
          departmentsSameRegion.push({
            name: department.nom,
            code: department.code,
            regionCode: department.codeRegion
          })
        })
        this.departmentsSameRegionList = [...departmentsSameRegion];
      }
    )
  }

}
