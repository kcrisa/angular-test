import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GeoApiService {

  constructor(
    private httpClient: HttpClient
  ) { }

  private setHeaders(): HttpHeaders {
    return new HttpHeaders();
  }

  handleErrors(response: any) {
    return response;
  }

  get(api_get_url: string): Observable<any> {

    return this.httpClient
      .get(environment.api_geo_url + '/' + api_get_url, { headers: this.setHeaders() })
      .pipe(catchError(errors => {
        return Observable.throw(errors);
      }),
        map(data => this.handleErrors(data)));
  }

  getAllDepartments() {
    return this.get("departements");
  }

  getDepartmentsFromRegionCode(regionCode: string) {
    return this.get("regions/" + regionCode + '/departements');
  }
}
