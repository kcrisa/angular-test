import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    redirectTo: '/geoApi',
    pathMatch: 'full'
  },
  {
    path: 'geoApi',
    loadChildren: './components/geo-api/geo-api.module#GeoApiModule'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
